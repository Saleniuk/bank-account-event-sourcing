<style>
    .bank-account {
        padding: 10px;
        border: 1px dashed black;
        margin-top: 30px;
    }

    .closed {
        position: absolute;
        top: 30px;
        bottom: 0;
        left: 15px;
        right: 15px;
        background: rgba(255, 0, 0, 0.13);
    }

    .btn {
        cursor: pointer;
    }

    .amount {
        margin-bottom: 15px;
    }

    .transactions-button {
        margin-top: 10px;
    }
</style>

<div class="row">
@foreach ($bank_accounts as $bankAccount)
    <div class="col-md-4">
        <div class="bank-account">
            <p><b>Nazwa: </b>{{ $bankAccount['name'] }}</p>
            <p><b>Imię: </b>{{ $bankAccount['first_name'] }}</p>
            <p><b>Nazwisko: </b>{{ $bankAccount['last_name'] }}</p>
            <p><b>Saldo: </b>{{ $bankAccount['balance'] }}</p>

            <div class="amount">
                <input type="text" id="amount-{{ $bankAccount['id'] }}" placeholder="Kwota"/>
            </div>
            <button data-id="{{ $bankAccount['id'] }}" class="btn btn-success load-bank-account">Doładuj</button>
            <button data-id="{{ $bankAccount['id'] }}" class="btn btn-warning unload-bank-account">Wypłać</button>
            <button data-id="{{ $bankAccount['id'] }}" class="btn btn-danger close-bank-account">Zamknij konto</button>
            <div class="transactions-button">
                <button data-id="{{ $bankAccount['id'] }}" class="btn btn-info show-transactions-bank-account">Pokaż listę transakcji</button>
            </div>
            @if ($bankAccount['status'] == 2)
                <div class="closed"></div>
            @endif
        </div>
        <!-- Button trigger modal -->
        <button id="modal-button-{{$bankAccount['id']}}" type="button" class="btn btn-primary btn-lg" data-toggle="modal" style="display: none" data-target="#myModal-{{$bankAccount['id']}}">
            Launch demo modal
        </button>

        <!-- Modal -->
        <div class="modal fade" id="myModal-{{$bankAccount['id']}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Transakcje</h4>
                    </div>
                    <div class="modal-body">
                        <div id="modal-body-{{$bankAccount['id']}}"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
</div>