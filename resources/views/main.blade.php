<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>


        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

    </head>
    <style>
        .my-container {
            margin-top: 10%;
        }

        #bank-accounts {
            margin-top: 40px;
            margin-bottom: 40px;
        }

        .alert-container {
            position: fixed;
            text-align: center;
            width: 80%;
            margin-left: 10%;
            margin-right: 10%;
            z-index: 1000;
            margin-top: 20px;
            top: 0;
        }
    </style>
    <body>
        <div class="alert-container">
            <div id="show-alert"></div>
        </div>
        <div class="container">
            <div class="my-container">
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <form id="register-form">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nazwa</label>
                                <input id="name" class="form-control" placeholder="Nazwa">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Imię</label>
                                <input id="first-name" class="form-control" id="exampleInputPassword1" placeholder="Imię">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nazwisko</label>
                                <input id="last-name" class="form-control" id="exampleInputPassword1" placeholder="Nazwisko">
                            </div>

                            <button type="submit" class="btn btn-default">Zarejestruj</button>
                        </form>
                    </div>
                </div>
            </div>
            <div id="bank-accounts"></div>
        </div>
    <script>
        jQuery(document).ready(function() {
           jQuery('#register-form').submit(function(event) {
               event.preventDefault();
               var name = jQuery('#name').val();
               var firstName = jQuery('#first-name').val();
               var lastName = jQuery('#last-name').val();
               jQuery.post( "http://event-sourcing.local/api/bank-account",  { user_id: 1, name: name, first_name: firstName, last_name: lastName }, function( data ) {
                   getBankAccounts();
                   showAlert('success', 'Konto zostało utworzone.');
               });
           });

            jQuery('body').on('click', '.load-bank-account', function(event) {
                var id = jQuery(this).data('id');
                var amount = jQuery('#amount-' + id).val();
                jQuery.post('http://event-sourcing.local/api/bank-account/' + id + '/load', { amount: amount }, function(data) {
                    getBankAccounts();
                    showAlert('success', 'Konto zostało doładowane.');
                });
            });

            jQuery('body').on('click', '.unload-bank-account', function(event) {
                var id = jQuery(this).data('id');
                var amount = jQuery('#amount-' + id).val();
                jQuery.post('http://event-sourcing.local/api/bank-account/' + id + '/unload', { amount: amount }, function(data) {
                    getBankAccounts();
                    showAlert('info', 'Środki zostały wypłacone z konta.');
                });
            });

            jQuery('body').on('click', '.close-bank-account', function(event) {
                var id = jQuery(this).data('id');
                jQuery.post('http://event-sourcing.local/api/bank-account/' + id + '/close', function(data) {
                    getBankAccounts();
                    showAlert('danger', 'Konto zostało zamknięte.');
                });
            });

            jQuery('body').on('click', '.show-transactions-bank-account', function(event) {
                var id = jQuery(this).data('id');
                jQuery.get('http://event-sourcing.local/api/bank-account/' + id + '/transactions', function(data) {
                    jQuery.post('http://event-sourcing.local/api/get-bank-account-transactions', data, function( response ) {
                        console.log(id);
                        jQuery('#modal-body-' + id).html(response);
                        jQuery('#modal-button-' + id).click();
                    })
                });
            });

            getBankAccounts();

        });

        function getBankAccounts() {
            jQuery.get('http://event-sourcing.local/api/bank-account', function( data ) {
               jQuery.post('http://event-sourcing.local/api/get-bank-accounts', data, function( response ) {
                   jQuery('#bank-accounts').html(response);
               })
            });
        }

        function showAlert(type, text) {
            var showAlert = jQuery('#show-alert');
            showAlert.html(
                '<div class="alert alert-' + type + '">' + text + '</div>'
            );
            showAlert.fadeIn();
            setTimeout(function() {
                showAlert.fadeOut();
            }, 3000);
        }

    </script>
    </body>
</html>
