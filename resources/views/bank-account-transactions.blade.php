
<div class="row">
        <table class="table">
            <thead>
            <tr>
                <th>Kwota</th>
                <th>Typ</th>
                {{--<th>Opis</th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach ($transactions as $transaction)
                <tr>
                <td>{{$transaction['amount']}} zł</td>
                <td>@if($transaction['type'] == 'load')Wpłata @else Wypłata @endif</td>
{{--                <td>{{$transaction['description']}}</td>--}}
            </tr>
            @endforeach

            </tbody>
        </table>
</div>