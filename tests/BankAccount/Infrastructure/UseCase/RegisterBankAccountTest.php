<?php

declare(strict_types = 1);

namespace Tests;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\RegisterBankAccountCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory\BankAccountFactoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\BankAccountFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\BankAccountRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase\RegisterBankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase\RegisterBankAccountInterface;
use Faker\Factory as Faker;

class RegisterBankAccountTest extends TestCase
{
    /** @var RegisterBankAccountInterface */
    protected $registerBankAccount;

    /** @var BankAccountFactoryInterface */
    protected $bankAccountFactory;

    /** @var BankAccountRepositoryInterface */
    protected $bankAccountRepository;

    protected $faker;

    public function setUp()
    {
        $this->faker = Faker::create();
        $this->bankAccountFactory = $this->getMockBuilder(BankAccountFactoryInterface::class)->getMock();
        $this->bankAccountRepository = $this->getMockBuilder(BankAccountRepositoryInterface::class)->getMock();
        $this->registerBankAccount = new RegisterBankAccount(
            $this->bankAccountRepository,
            $this->bankAccountFactory
        );
    }

    /** @test */
    public function it_should_register_the_bank_account()
    {
//        $registerBankAccountCommand = $this->getRegisterBankAccountCommand();
//        $bankAccount = $this->getBankAccount($registerBankAccountCommand);
//        $this->bankAccountFactory->expects($this->once())
//            ->method('create')->with([BankAccountFields::USER_ID => $registerBankAccountCommand->getUserId()])->willReturn($bankAccount);
//        $this->bankAccountRepository->expects($this->once())->method('register')->with($bankAccount);
//        $this->registerBankAccount->execute($registerBankAccountCommand);
    }

    private function getRegisterBankAccountCommand(): RegisterBankAccountCommand
    {
        return new RegisterBankAccountCommand(
            $this->faker->numberBetween(1, 100)
        );
    }

    private function getBankAccount(RegisterBankAccountCommand $command): BankAccount
    {
        return new BankAccount(
            0,
            $command->getUserId(),
            0,
            0
        );
    }
}