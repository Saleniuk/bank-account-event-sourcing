<?php

declare(strict_types = 1);

namespace Tests;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Gateway\BankAccountGatewayInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator\BankAccountHydratorInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\BankAccountFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\BankAccountRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Repository\BankAccountRepository;
use Faker\Factory as Faker;

class BankAccountRepositoryTest extends TestCase
{
    /** @var BankAccountRepositoryInterface */
    protected $bankAccountRepository;

    /** @var BankAccountHydratorInterface */
    protected $bankAccountHydrator;

    /** @var BankAccountGatewayInterface */
    protected $bankAccountGateway;

    protected $faker;

    public function setUp()
    {
        $this->faker = Faker::create();
        $this->bankAccountHydrator = $this->getMockBuilder(BankAccountHydratorInterface::class)->getMock();
        $this->bankAccountGateway = $this->getMockBuilder(BankAccountGatewayInterface::class)->getMock();
        $this->bankAccountRepository = new BankAccountRepository(
            $this->bankAccountHydrator,
            $this->bankAccountGateway
        );
    }

    /** @test */
    public function it_should_register_the_bank_account()
    {
        $bankAccount = $this->getBankAccount();
        $bankAccountData = $this->getBankAccountData($bankAccount);
        $id = $this->faker->numberBetween(1, 100);
        $this->bankAccountHydrator->expects($this->once())->method('extract')->with($bankAccount)->willReturn($bankAccountData);
        $this->bankAccountGateway->expects($this->once())->method('register')->with($bankAccountData)->willReturn($id);
        $returnId = $this->bankAccountRepository->register($bankAccount);
        $this->assertEquals($id, $returnId);
    }

    private function getBankAccount(): BankAccount
    {
        return new BankAccount(
            $this->faker->numberBetween(1, 100),
            $this->faker->numberBetween(1, 100),
            $this->faker->numberBetween(1, 100),
            $this->faker->numberBetween(1, 100)
        );
    }

    private function getBankAccountData(BankAccount $bankAccount): array
    {
        return [
            BankAccountFields::ID => $bankAccount->getId(),
            BankAccountFields::USER_ID => $bankAccount->getUserId(),
            BankAccountFields::BALANCE => $bankAccount->getBalance(),
            BankAccountFields::STATUS => $bankAccount->getStatus()
        ];
    }
}