<?php

declare(strict_types=1);

namespace Tests;

use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Gateway\EventStorageGateway;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Mapper\EventStorageFields;
use JakubSaleniuk\EventSourcing\Domain\Gateway\EventStorageGatewayInterface;
use Faker\Factory as Faker;

class EventStorageGatewayTest extends TestCase
{
    /** @var EventStorageGatewayInterface */
    protected $eventStorageGateway;

    protected $faker;

    public function setUp()
    {
        parent::setUp();
        $this->setDB();
        $this->faker = Faker::create();
        $this->eventStorageGateway = new EventStorageGateway();
    }

    /** @test */
    public function it_should_save_an_event_storage()
    {
        $eventStorageData = $this->getEventStorageData();
        $this->eventStorageGateway->save($eventStorageData);
    }

    private function getEventStorageData(): array
    {
        return [
            EventStorageFields::ID => $this->faker->numberBetween(1, 100),
            EventStorageFields::NAME => 'BankAccountWasRegistered',
            EventStorageFields::UUID => $this->faker->numberBetween(1, 100),
            EventStorageFields::COUNTER => $this->faker->numberBetween(1, 100),
            EventStorageFields::PAYLOAD => json_encode(['amount' => $this->faker->numberBetween(1, 100)]),
            EventStorageFields::METADATA => json_encode(['user_id' => $this->faker->numberBetween(1, 100)]),
            EventStorageFields::AGGREGATE_ID => $this->faker->numberBetween(1, 100)
        ];
    }
}