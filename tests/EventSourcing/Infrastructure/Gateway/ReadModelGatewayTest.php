<?php

declare(strict_types=1);

namespace Tests;

use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Gateway\ReadModelGateway;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Mapper\ReadModelFields;
use JakubSaleniuk\EventSourcing\Domain\Gateway\ReadModelGatewayInterface;
use Faker\Factory as Faker;

class ReadModelGatewayTest extends TestCase
{
    /** @var ReadModelGatewayInterface */
    protected $readModelGateway;

    protected $faker;

    public function setUp()
    {
        parent::setUp();
        $this->setDB();
        $this->faker = Faker::create();
        $this->readModelGateway = new ReadModelGateway();
    }

    /** @test */
    public function it_should_save_a_read_model()
    {
        $readModelData = $this->getReadModelData();
        $this->readModelGateway->save($readModelData);
    }

    private function getReadModelData(): array
    {
        return [
            ReadModelFields::ID => $this->faker->numberBetween(1, 100),
            ReadModelFields::NAME => 'bank-account',
            ReadModelFields::METADATA => json_encode(['user_id' => $this->faker->numberBetween(1, 100)]),
            ReadModelFields::PROJECTION => json_encode(['amount' => $this->faker->numberBetween(1, 100)])
        ];
    }
}