<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\CloseBankAccountCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\GetBankAccountTransactionsCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\LoadBankAccountCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\RegisterBankAccountCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\UnloadBankAccountCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\BankAccountFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase\GetBankAccountTransactionsInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Presenter\JSONGetBankAccountsPresenter;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Presenter\JSONGetBankAccountTransactionsPresenter;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase\CloseBankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase\GetBankAccounts;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase\GetBankAccountTransactions;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase\LoadBankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase\RegisterBankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase\UnloadBankAccount;

/**
 * Class BankAccountController
 * @package App\Http\Controllers
 */
class BankAccountController extends Controller
{
    /**
     * @param Request $request
     * @param RegisterBankAccount $registerBankAccount
     */
    public function post(Request $request, RegisterBankAccount $registerBankAccount)
    {
        $command = new RegisterBankAccountCommand(
            (int)$request->get('user_id'),
            $request->get('name'),
            $request->get('first_name'),
            $request->get('last_name')
        );
        $registerBankAccount->execute($command);
    }

    /**
     * @param GetBankAccounts $query
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function getAll(GetBankAccounts $query)
    {
        $presenter = new JSONGetBankAccountsPresenter();
        try {
            $query->execute($presenter);
            return $presenter->getView();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBankAccounts(Request $request)
    {
        return view('bank-accounts', $request->all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBankAccountTransactions(Request $request)
    {
        return view('bank-account-transactions', $request->all());
    }

    /**
     * @param $id
     * @param Request $request
     * @param LoadBankAccount $loadBankAccount
     */
    public function load($id, Request $request, LoadBankAccount $loadBankAccount)
    {
        $command = new LoadBankAccountCommand(
            $id,
            (int)$request->get('amount')
        );
        $loadBankAccount->execute($command);
    }

    /**
     * @param $id
     * @param Request $request
     * @param UnloadBankAccount $unloadBankAccount
     */
    public function unload($id, Request $request, UnloadBankAccount $unloadBankAccount)
    {
        $command = new UnloadBankAccountCommand(
            $id,
            (int)$request->get('amount')
        );
        $unloadBankAccount->execute($command);
    }

    /**
     * @param $id
     * @param CloseBankAccount $closeBankAccount
     */
    public function close($id, CloseBankAccount $closeBankAccount)
    {
        $command = new CloseBankAccountCommand($id);
        $closeBankAccount->execute($command);
    }

    /**
     * @param $id
     * @param GetBankAccountTransactions $query
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function getTransactions($id, GetBankAccountTransactions $query)
    {
        $presenter = new JSONGetBankAccountTransactionsPresenter();
        $command = new GetBankAccountTransactionsCommand($id);

        try {
            $query->execute($command, $presenter);
            return $presenter->getView();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
    }
}