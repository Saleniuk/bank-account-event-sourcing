<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EventStorage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_storage';
}
