<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReadModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'read_model';
}
