<?php

namespace App\Providers;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory\BankAccountFactoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory\EventFactoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Gateway\BankAccountGatewayInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator\BankAccountHydratorInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator\TransactionHydratorInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\BankAccountRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\TransactionRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\EventListener\CreatingBankAccountEventListener;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\EventListener\CreatingBankAccountTransactionEventListener;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory\BankAccountFactory;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory\BankAccountProjectorFactory;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory\BankAccountTransactionsFactory;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory\BankAccountTransactionsProjectorFactory;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory\EventFactory;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Hydrator\BankAccountHydrator;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Hydrator\TransactionHydrator;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Repository\BankAccountRepository;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Repository\TransactionRepository;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory\EventAggregateFactory;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory\EventStorageFactory;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory\ReadModelFactory;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Hydrator\ReadModelHydrator;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Event\EventBus;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Hydrator\EventStorageHydrator;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Gateway\EventStorageGateway;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Gateway\ReadModelGateway;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Repository\EventStorageRepository;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Repository\ReadModelRepository;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Service\CreateBankAccountProjectionService;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Service\CreateProjectionService;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Service\DispatchEventService;
use JakubSaleniuk\EventSourcing\Domain\Event\EventBusInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\EventAggregateFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\EventStorageFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\ReadModelFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Gateway\EventStorageGatewayInterface;
use JakubSaleniuk\EventSourcing\Domain\Gateway\ReadModelGatewayInterface;
use JakubSaleniuk\EventSourcing\Domain\Hydrator\EventStorageHydratorInterface;
use JakubSaleniuk\EventSourcing\Domain\Hydrator\ReadModelHydratorInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\EventStorageRepositoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\ReadModelRepositoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Service\DispatchEventServiceInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        CreatingBankAccountEventListener::class,
        CreatingBankAccountTransactionEventListener::class
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->subscribe as $subscriber) {
            Event::subscribe($subscriber);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EventStorageRepositoryInterface::class, EventStorageRepository::class);
        $this->app->bind(EventStorageGatewayInterface::class, EventStorageGateway::class);
        $this->app->bind(EventStorageHydratorInterface::class, EventStorageHydrator::class);
        $this->app->bind(EventBusInterface::class, EventBus::class);
        $this->app->bind(EventStorageFactoryInterface::class, EventStorageFactory::class);
        $this->app->bind(EventFactoryInterface::class, EventFactory::class);
        $this->app->bind(EventAggregateFactoryInterface::class, EventAggregateFactory::class);

        $this->app->bind(ReadModelRepositoryInterface::class, ReadModelRepository::class);
        $this->app->bind(ReadModelHydratorInterface::class, ReadModelHydrator::class);
        $this->app->bind(ReadModelGatewayInterface::class, ReadModelGateway::class);
        $this->app->bind(ReadModelFactoryInterface::class, ReadModelFactory::class);

        $this->app->bind(BankAccountRepositoryInterface::class, BankAccountRepository::class);
        $this->app->bind(BankAccountHydratorInterface::class, BankAccountHydrator::class);
        $this->app->bind(BankAccountFactoryInterface::class, BankAccountFactory::class);
        $this->app->bind(BankAccountFactoryInterface::class, BankAccountFactory::class);

        $this->app->bind(TransactionRepositoryInterface::class, TransactionRepository::class);
        $this->app->bind(TransactionHydratorInterface::class, TransactionHydrator::class);

        $this->app->bind(DispatchEventServiceInterface::class, DispatchEventService::class);

        $this->app->bind(CreatingBankAccountEventListener::class, function (Application $application) {
            return new CreatingBankAccountEventListener(
                $application->make(ReadModelRepositoryInterface::class),
                $application->make(ReadModelFactoryInterface::class),
                new CreateProjectionService(
                    $application->make(EventStorageRepositoryInterface::class),
                    $application->make(BankAccountProjectorFactory::class),
                    $application->make(EventFactoryInterface::class),
                    $application->make(BankAccountFactory::class),
                    $application->make(EventAggregateFactoryInterface::class)
                )
            );
        });

        $this->app->bind(CreatingBankAccountTransactionEventListener::class, function (Application $application) {
            return new CreatingBankAccountTransactionEventListener(
                $application->make(ReadModelRepositoryInterface::class),
                $application->make(ReadModelFactoryInterface::class),
                new CreateProjectionService(
                    $application->make(EventStorageRepositoryInterface::class),
                    $application->make(BankAccountTransactionsProjectorFactory::class),
                    $application->make(EventFactoryInterface::class),
                    $application->make(BankAccountTransactionsFactory::class),
                    $application->make(EventAggregateFactoryInterface::class)
                )
            );
        });
    }
}
