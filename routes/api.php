<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('bank-account', 'BankAccountController@post');
Route::post('bank-account/{id}/load', 'BankAccountController@load');
Route::post('bank-account/{id}/unload', 'BankAccountController@unload');
Route::post('bank-account/{id}/close', 'BankAccountController@close');
Route::get('bank-account/{id}/transactions', 'BankAccountController@getTransactions');
Route::get('bank-account', 'BankAccountController@getAll');
Route::post('get-bank-accounts', 'BankAccountController@getBankAccounts');
Route::post('get-bank-account-transactions', 'BankAccountController@getBankAccountTransactions');
