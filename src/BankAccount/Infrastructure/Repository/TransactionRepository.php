<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Repository;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\Transaction;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\TransactionFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\TransactionRepositoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Entity\ReadModel;
use JakubSaleniuk\EventSourcing\Domain\Hydrator\ReadModelHydratorInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\ReadModelRepositoryInterface;

class TransactionRepository implements TransactionRepositoryInterface
{
    /** @var ReadModelRepositoryInterface */
    protected $readModelRepository;

    /** @var ReadModelHydratorInterface */
    protected $readModelHydrator;

    /**
     * TransactionRepository constructor.
     * @param ReadModelRepositoryInterface $readModelRepository
     * @param ReadModelHydratorInterface $readModelHydrator
     */
    public function __construct(ReadModelRepositoryInterface $readModelRepository, ReadModelHydratorInterface $readModelHydrator)
    {
        $this->readModelRepository = $readModelRepository;
        $this->readModelHydrator = $readModelHydrator;
    }

    public function findByBankAccountId(string $bankAccountId): array
    {
        $readModels = $this->readModelRepository->find('bank-account-transactions', ['metadata' => [
            'id' => (string)$bankAccountId
        ]]);
        $transactions = [];

        if ($readModels && $readModels[0]) {
            $readModel = $readModels[0];

            /** @var ReadModel $readModel */
            $transactionsData = $readModel->getProjection();

            foreach ($transactionsData as $transaction) {
                array_push($transactions, new Transaction(
                    $transaction[TransactionFields::ID] ?? 0,
                    $transaction[TransactionFields::AMOUNT] ?? 0,
                    $transaction[TransactionFields::DESCRIPTION] ?? '',
                    $transaction[TransactionFields::TYPE] ?? ''
                ));
            }
        }

        return $transactions;
    }
}