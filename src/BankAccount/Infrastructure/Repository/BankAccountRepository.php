<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Repository;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator\BankAccountHydratorInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\BankAccountFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\BankAccountRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasClosed;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasLoaded;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasRegistered;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasUnloaded;
use JakubSaleniuk\EventSourcing\Domain\Entity\ReadModel;
use JakubSaleniuk\EventSourcing\Domain\Repository\ReadModelRepositoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Service\DispatchEventServiceInterface;

/**
 * Class BankAccountRepository
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Repository
 */
class BankAccountRepository implements BankAccountRepositoryInterface
{
    /** @var BankAccountHydratorInterface */
    protected $bankAccountHydrator;

    /** @var ReadModelRepositoryInterface */
    protected $readModelRepository;

    /** @var DispatchEventServiceInterface */
    protected $dispatchEventService;

    /**
     * BankAccountRepository constructor.
     * @param BankAccountHydratorInterface $bankAccountHydrator
     * @param ReadModelRepositoryInterface $readModelRepository
     * @param DispatchEventServiceInterface $dispatchEventService
     */
    public function __construct(
        BankAccountHydratorInterface $bankAccountHydrator,
        ReadModelRepositoryInterface $readModelRepository,
        DispatchEventServiceInterface $dispatchEventService
    ) {
        $this->bankAccountHydrator = $bankAccountHydrator;
        $this->readModelRepository = $readModelRepository;
        $this->dispatchEventService = $dispatchEventService;
    }

    /**
     * @param int $id
     * @return BankAccount
     */
    public function findById(int $id): BankAccount
    {

    }

    /**
     * @param int $userId
     * @return array
     */
    public function findByUserId(int $userId): array
    {
        $readModels = $this->readModelRepository->find('bank-account', ['metadata' => [
            'user_id' => (string)$userId
        ]]);

        $bankAccounts = [];

        /** @var ReadModel $readModel */
        foreach ($readModels as $readModel) {
            $bankAccountData = $readModel->getProjection();

            array_push($bankAccounts, new BankAccount(
                $bankAccountData[BankAccountFields::ID],
                $bankAccountData[BankAccountFields::NAME],
                $bankAccountData[BankAccountFields::FIRST_NAME],
                $bankAccountData[BankAccountFields::LAST_NAME],
                $bankAccountData[BankAccountFields::USER_ID],
                $bankAccountData[BankAccountFields::BALANCE],
                $bankAccountData[BankAccountFields::STATUS]
            ));
        }

        return $bankAccounts;
    }

    /**
     * @param BankAccount $bankAccount
     * @return int
     */
    public function register(BankAccount $bankAccount)
    {
        $aggregateId = uniqid();
        $bankAccountWasRegistered = new BankAccountWasRegistered(
            $aggregateId,
            $bankAccount->getUserId(),
            $bankAccount->getName(),
            $bankAccount->getFirstName(),
            $bankAccount->getLastName()
        );
        $this->dispatchEventService->dispatch($bankAccountWasRegistered);

        return $bankAccountWasRegistered->getId();
    }

    /**
     * @param string $bankAccountId
     * @param int $amount
     * @return void
     */
    public function load(string $bankAccountId, int $amount)
    {
        $event = new BankAccountWasLoaded($bankAccountId, $amount);
        $this->dispatchEventService->dispatch($event);
    }

    /**
     * @param string $bankAccountId
     * @param int $amount
     */
    public function unload(string $bankAccountId, int $amount)
    {
        $event = new BankAccountWasUnloaded($bankAccountId, $amount);
        $this->dispatchEventService->dispatch($event);
    }

    /**
     * @param string $bankAccountId
     */
    public function close(string $bankAccountId)
    {
        $event = new BankAccountWasClosed($bankAccountId);
        $this->dispatchEventService->dispatch($event);
    }
}