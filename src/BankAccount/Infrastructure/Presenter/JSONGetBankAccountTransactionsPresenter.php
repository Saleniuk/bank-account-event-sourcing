<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Presenter;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\TransactionFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter\GetBankAccountTransactionsPresenterInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response\GetBankAccountTransactionsResponse;

/**
 * Class JSONGetBankAccountTransactionsPresenter
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Presenter
 */
class JSONGetBankAccountTransactionsPresenter implements GetBankAccountTransactionsPresenterInterface
{
    /** @var mixed */
    private $view;

    /**
     * @param GetBankAccountTransactionsResponse $response
     * @return void
     */
    public function present(GetBankAccountTransactionsResponse $response)
    {
        $this->view = response()->json([
            TransactionFields::TRANSACTIONS => $response->getTransactions()
        ]);
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }
}