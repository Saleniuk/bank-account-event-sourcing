<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Presenter;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\BankAccountFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter\GetBankAccountsPresenterInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response\GetBankAccountsResponse;

/**
 * Class JSONGetBankAccountsPresenter
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Presenter
 */
class JSONGetBankAccountsPresenter implements GetBankAccountsPresenterInterface
{
    /** @var mixed */
    private $view;

    /**
     * @param GetBankAccountsResponse $response
     * @return void
     */
    public function present(GetBankAccountsResponse $response)
    {
        $this->view = response()->json([
            BankAccountFields::BANK_ACCOUNTS => $response->getBankAccounts()
        ]);
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }
}