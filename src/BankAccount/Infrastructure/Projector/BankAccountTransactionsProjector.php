<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Projector;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\Transaction;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Aggregate\TransactionAggregate;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasClosed;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasLoaded;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasRegistered;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasUnloaded;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\EventAggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\ProjectorInterface;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Class BankAccountTransactionsProjector
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Projector
 */
class BankAccountTransactionsProjector implements ProjectorInterface
{
    /** @var TransactionAggregate */
    private $transactions;

    /**
     * BankAccountTransactionsProjector constructor.
     * @param AggregateInterface $transactions
     */
    public function __construct(AggregateInterface $transactions)
    {
        $this->transactions = $transactions;
    }

    /**
     * @return string
     */
    public function getProjectionName(): string
    {
        return 'bank-account-transactions';
    }

    /**
     * @param EventInterface $event
     * @return AggregateInterface
     */
    public function applyEvent(EventInterface $event): AggregateInterface
    {
        $methodName = 'apply' . $event->getEventName();
        $this->$methodName($event);

        return $this->transactions;
    }

    /**
     * @param EventAggregateInterface $eventAggregate
     * @return AggregateInterface
     */
    public function applyEvents(EventAggregateInterface $eventAggregate): AggregateInterface
    {
        foreach ($eventAggregate->getEvents() as $event) {
            $this->applyEvent($event);
        }

        return $this->transactions;
    }

    /**
     * @param BankAccountWasLoaded $event
     */
    public function applyBankAccountWasLoaded(BankAccountWasLoaded $event)
    {
        $transaction = new Transaction(0, $event->getAmount(), '', Transaction::LOAD);
        $this->transactions->addTransaction($transaction);
    }

    /**
     * @param BankAccountWasUnloaded $event
     */
    public function applyBankAccountWasUnloaded(BankAccountWasUnloaded $event)
    {
        $transaction = new Transaction(0, $event->getAmount(), '', Transaction::UNLOAD);
        $this->transactions->addTransaction($transaction);
    }

    /**
     * @param BankAccountWasRegistered $event
     */
    public function applyBankAccountWasRegistered(BankAccountWasRegistered $event) {}

    /**
     * @param BankAccountWasClosed $event
     */
    public function applyBankAccountWasClosed(BankAccountWasClosed $event) {}
}