<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Projector;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasClosed;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasLoaded;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasRegistered;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasUnloaded;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\EventAggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\ProjectorInterface;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Class BankAccountProjector
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Projector
 */
class BankAccountProjector implements ProjectorInterface
{
    /** @var BankAccount */
    protected $bankAccount;

    /**
     * BankAccountProjector constructor.
     * @param AggregateInterface $bankAccount
     */
    public function __construct(AggregateInterface $bankAccount)
    {
        $this->bankAccount = $bankAccount;
    }

    /**
     * @return string
     */
    public function getProjectionName(): string
    {
        return 'bank-account';
    }

    /**
     * @param EventInterface $event
     * @return AggregateInterface
     */
    public function applyEvent(EventInterface $event): AggregateInterface
    {
        $methodName = 'apply' . $event->getEventName();
        $this->$methodName($event);

        return $this->bankAccount;
    }

    /**
     * @param EventAggregateInterface $eventAggregate
     * @return AggregateInterface
     */
    public function applyEvents(EventAggregateInterface $eventAggregate): AggregateInterface
    {
        foreach ($eventAggregate->getEvents() as $event) {
            $this->applyEvent($event);
        }

        return $this->bankAccount;
    }

    /**
     * @param BankAccountWasRegistered $event
     */
    public function applyBankAccountWasRegistered(BankAccountWasRegistered $event)
    {
        $bankAccount = new BankAccount(
            $event->getAggregateId(),
            $event->getName(),
            $event->getFirstName(),
            $event->getLastName(),
            $event->getUserId(),
            0,
            1
        );
        $this->bankAccount = $bankAccount;
    }

    /**
     * @param BankAccountWasLoaded $event
     */
    public function applyBankAccountWasLoaded(BankAccountWasLoaded $event)
    {
        $this->bankAccount->loadBankAccount($event->getAmount());
    }

    /**
     * @param BankAccountWasUnloaded $event
     */
    public function applyBankAccountWasUnloaded(BankAccountWasUnloaded $event)
    {
        $this->bankAccount->unloadBankAccount($event->getAmount());
    }

    /**
     * @param BankAccountWasClosed $event
     */
    public function applyBankAccountWasClosed(BankAccountWasClosed $event)
    {
        $this->bankAccount->closeBankAccount();
    }
}