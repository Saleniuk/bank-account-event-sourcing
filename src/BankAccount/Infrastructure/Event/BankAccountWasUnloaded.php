<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper\BankAccountWasUnloadedFields;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Class BankAccountWasUnloaded
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event
 */
class BankAccountWasUnloaded implements EventInterface
{
    /** @var int */
    private $id;

    /** @var string */
    private $aggregateId;

    /** @var int */
    private $amount;

    /**
     * BankAccountWasUnloaded constructor.
     * @param string $aggregateId
     * @param int $amount
     */
    public function __construct($aggregateId, $amount)
    {
        $this->aggregateId = $aggregateId;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            BankAccountWasUnloadedFields::AGGREGATE_ID => $this->getAggregateId(),
            BankAccountWasUnloadedFields::AMOUNT => $this->getAmount()
        ];
    }
}