<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper\BankAccountWasRegisteredFields;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Class BankAccountWasRegistered
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event
 */
class BankAccountWasRegistered implements EventInterface
{
    /** @var int */
    private $id;

    /** @var string */
    private $aggregateId;

    /** @var int */
    private $userId;

    /** @var string */
    private $name;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /**
     * BankAccountWasRegistered constructor.
     * @param $aggregateId
     * @param $userId
     * @param $name
     * @param $firstName
     * @param $lastName
     */
    public function __construct($aggregateId, $userId, $name, $firstName, $lastName)
    {
        $this->aggregateId = $aggregateId;
        $this->userId = $userId;
        $this->name = $name;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return string
     */
    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            BankAccountWasRegisteredFields::AGGREGATE_ID => $this->getAggregateId(),
            BankAccountWasRegisteredFields::USER_ID => $this->getUserId(),
            BankAccountWasRegisteredFields::NAME => $this->getName(),
            BankAccountWasRegisteredFields::FIRST_NAME => $this->getFirstName(),
            BankAccountWasRegisteredFields::LAST_NAME => $this->getLastName()
        ];
    }
}