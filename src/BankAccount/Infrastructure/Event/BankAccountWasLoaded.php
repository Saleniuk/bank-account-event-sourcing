<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper\BankAccountWasLoadedFields;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Class BankAccountWasLoaded
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event
 */
class BankAccountWasLoaded implements EventInterface
{
    /** @var int */
    private $id;

    /** @var string */
    private $aggregateId;

    /** @var int */
    private $amount;

    /**
     * BankAccountWasLoaded constructor.
     * @param string $aggregateId
     * @param int $amount
     */
    public function __construct($aggregateId, $amount)
    {
        $this->aggregateId = $aggregateId;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            BankAccountWasLoadedFields::AGGREGATE_ID => $this->getAggregateId(),
            BankAccountWasLoadedFields::AMOUNT => $this->getAmount()
        ];
    }
}