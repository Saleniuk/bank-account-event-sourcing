<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper\BankAccountWasClosedFields;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Class BankAccountWasClosed
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event
 */
class BankAccountWasClosed implements EventInterface
{
    /** @var int */
    private $id;

    /** @var string */
    private $aggregateId;

    /**
     * BankAccountWasClosed constructor.
     * @param string $aggregateId
     */
    public function __construct(string $aggregateId)
    {
        $this->aggregateId = $aggregateId;
    }

    /**
     * @return string
     */
    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            BankAccountWasClosedFields::AGGREGATE_ID => $this->getAggregateId()
        ];
    }
}