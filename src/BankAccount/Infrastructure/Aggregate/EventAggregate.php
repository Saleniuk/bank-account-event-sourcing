<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Aggregate;

use JakubSaleniuk\EventSourcing\Domain\Aggregate\EventAggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Class EventAggregate
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Aggregate
 */
class EventAggregate implements EventAggregateInterface
{
    /**
     * @var array
     */
    private $events = [];

    /**
     * @param EventInterface $event
     * @return void
     */
    public function addEvent(EventInterface $event)
    {
        array_push($this->events, $event);
    }

    /**
     * @return array
     */
    public function getEvents(): array
    {
        return $this->events;
    }
}