<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Aggregate;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\Transaction;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\TransactionFields;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;

/**
 * Class TransactionAggregate
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Aggregate
 */
class TransactionAggregate implements AggregateInterface
{
    /** @var array */
    private $transactions = [];

    /**
     * @param Transaction $transaction
     */
    public function addTransaction(Transaction $transaction)
    {
        array_push($this->transactions, $transaction);
    }

    /**
     * @return array
     */
    public function getTransactions(): array
    {
        return $this->transactions;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $transactions = [];
        /** @var Transaction $transaction */
        foreach ($this->transactions as $transaction) {
            array_push($transactions, [
                TransactionFields::ID => $transaction->getId(),
                TransactionFields::AMOUNT => $transaction->getAmount(),
                TransactionFields::DESCRIPTION => $transaction->getDescription(),
                TransactionFields::TYPE => $transaction->getType()
            ]);
        }

        return $transactions;
    }
}