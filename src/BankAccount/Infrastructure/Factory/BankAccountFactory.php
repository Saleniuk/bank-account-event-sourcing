<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory\BankAccountFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\AggregateFactoryInterface;

/**
 * Class BankAccountFactory
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory
 */
class BankAccountFactory implements BankAccountFactoryInterface, AggregateFactoryInterface
{
    /**
     * @param string $name
     * @param string $firstName
     * @param string $lastName
     * @param int $userId
     * @return BankAccount
     */
    public function create(string $name, string $firstName, string $lastName, int $userId): BankAccount
    {
        return new BankAccount(
            0,
            $name,
            $firstName,
            $lastName,
            $userId,
            0,
            0
        );
    }

    /**
     * @return AggregateInterface
     */
    public function createAggregate(): AggregateInterface
    {
        return new BankAccount(
            0,
            '',
            '',
            '',
            0,
            0,
            0
        );
    }
}