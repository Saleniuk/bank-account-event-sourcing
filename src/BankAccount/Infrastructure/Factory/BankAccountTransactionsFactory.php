<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory\BankAccountTransactionsFactoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Aggregate\TransactionAggregate;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\AggregateFactoryInterface;

/**
 * Class BankAccountTransactionsFactory
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory
 */
class BankAccountTransactionsFactory implements BankAccountTransactionsFactoryInterface, AggregateFactoryInterface
{
    /**
     * @return AggregateInterface
     */
    public function create(): AggregateInterface
    {
        return new TransactionAggregate();
    }

    /**
     * @return AggregateInterface
     */
    public function createAggregate(): AggregateInterface
    {
        return new TransactionAggregate();
    }
}