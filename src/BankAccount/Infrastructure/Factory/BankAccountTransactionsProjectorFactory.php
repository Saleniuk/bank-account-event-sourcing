<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Projector\BankAccountTransactionsProjector;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\ProjectorInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\ProjectorFactoryInterface;

/**
 * Class BankAccountTransactionsProjectorFactory
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory
 */
class BankAccountTransactionsProjectorFactory implements ProjectorFactoryInterface
{
    /**
     * @param AggregateInterface $aggregate
     * @return ProjectorInterface
     */
    public function create(AggregateInterface $aggregate): ProjectorInterface
    {
        return new BankAccountTransactionsProjector($aggregate);
    }
}