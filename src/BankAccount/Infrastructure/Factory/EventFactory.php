<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory\EventFactoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasClosed;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasLoaded;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasRegistered;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasUnloaded;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper\BankAccountWasClosedFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper\BankAccountWasLoadedFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper\BankAccountWasRegisteredFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper\BankAccountWasUnloadedFields;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Class EventFactory
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory
 */
class EventFactory implements EventFactoryInterface
{
    /**
     * @param string $name
     * @param array $eventData
     * @return EventInterface
     */
    public function create(string $name, array $eventData): EventInterface
    {
        switch ($name) {
            case 'BankAccountWasRegistered': {
                return $this->createBankAccountWasRegistered($eventData);
            }
            case 'BankAccountWasLoaded': {
                return $this->createBankAccountWasLoaded($eventData);
            }
            case 'BankAccountWasUnloaded': {
                return $this->createBankAccountUnloaded($eventData);
            }
            case 'BankAccountWasClosed': {
                return $this->createBankAccountWasClosed($eventData);
            }
        }
    }

    /**
     * @param array $eventData
     * @return BankAccountWasRegistered
     */
    public function createBankAccountWasRegistered(array $eventData): BankAccountWasRegistered
    {
        return new BankAccountWasRegistered(
            $eventData[BankAccountWasRegisteredFields::AGGREGATE_ID] ?? '',
            $eventData[BankAccountWasRegisteredFields::USER_ID] ?? 0,
            $eventData[BankAccountWasRegisteredFields::NAME] ?? '',
            $eventData[BankAccountWasRegisteredFields::FIRST_NAME] ?? '',
            $eventData[BankAccountWasRegisteredFields::LAST_NAME] ?? ''
        );
    }

    /**
     * @param array $eventData
     * @return BankAccountWasLoaded
     */
    public function createBankAccountWasLoaded(array $eventData): BankAccountWasLoaded
    {
        return new BankAccountWasLoaded(
            $eventData[BankAccountWasLoadedFields::AGGREGATE_ID] ?? '',
            $eventData[BankAccountWasLoadedFields::AMOUNT] ?? 0
        );
    }

    /**
     * @param array $eventData
     * @return BankAccountWasUnloaded
     */
    public function createBankAccountUnloaded(array $eventData): BankAccountWasUnloaded
    {
        return new BankAccountWasUnloaded(
            $eventData[BankAccountWasUnloadedFields::AGGREGATE_ID] ?? '',
            $eventData[BankAccountWasUnloadedFields::AMOUNT] ?? 0
        );
    }

    /**
     * @param array $eventData
     * @return BankAccountWasClosed
     */
    public function createBankAccountWasClosed(array $eventData): BankAccountWasClosed
    {
        return new BankAccountWasClosed(
            $eventData[BankAccountWasClosedFields::AGGREGATE_ID] ?? ''
        );
    }
}