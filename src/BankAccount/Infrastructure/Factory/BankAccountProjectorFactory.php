<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Projector\BankAccountProjector;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\ProjectorInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\ProjectorFactoryInterface;

/**
 * Class BankAccountProjectorFactory
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Factory
 */
class BankAccountProjectorFactory implements ProjectorFactoryInterface
{
    /**
     * @param AggregateInterface $aggregate
     * @return ProjectorInterface
     */
    public function create(AggregateInterface $aggregate): ProjectorInterface
    {
        return new BankAccountProjector($aggregate);
    }
}