<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\LoadBankAccountCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\BankAccountRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase\LoadBankAccountInterface;

/**
 * Class LoadBankAccount
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase
 */
class LoadBankAccount implements LoadBankAccountInterface
{
    /** @var BankAccountRepositoryInterface */
    protected $bankAccountRepository;

    /**
     * LoadBankAccount constructor.
     * @param BankAccountRepositoryInterface $bankAccountRepository
     */
    public function __construct(BankAccountRepositoryInterface $bankAccountRepository)
    {
        $this->bankAccountRepository = $bankAccountRepository;
    }

    /**
     * @param LoadBankAccountCommand $command
     * @return void
     */
    public function execute(LoadBankAccountCommand $command)
    {
        $this->bankAccountRepository->load($command->getBankAccountId(), $command->getAmount());
    }
}