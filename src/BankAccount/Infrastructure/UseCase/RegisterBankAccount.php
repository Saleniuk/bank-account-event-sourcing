<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\RegisterBankAccountCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory\BankAccountFactoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\BankAccountFields;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\BankAccountRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase\RegisterBankAccountInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasRegistered;
use JakubSaleniuk\EventSourcing\Domain\Entity\EventStorage;
use JakubSaleniuk\EventSourcing\Domain\Event\EventBusInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\EventStorageRepositoryInterface;

/**
 * Class RegisterBankAccount
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase\RegisterBankAccount
 */
class RegisterBankAccount implements RegisterBankAccountInterface
{
    /** @var BankAccountRepositoryInterface */
    protected $bankAccountRepository;

    /** @var BankAccountFactoryInterface */
    protected $bankAccountFactory;

    /**
     * RegisterBankAccount constructor.
     * @param BankAccountRepositoryInterface $bankAccountRepository
     * @param BankAccountFactoryInterface $bankAccountFactory
     */
    public function __construct(
        BankAccountRepositoryInterface $bankAccountRepository,
        BankAccountFactoryInterface $bankAccountFactory
    ) {
        $this->bankAccountRepository = $bankAccountRepository;
        $this->bankAccountFactory = $bankAccountFactory;
    }

    /**
     * @param RegisterBankAccountCommand $command
     * @return void
     */
    public function execute(RegisterBankAccountCommand $command)
    {
        $bankAccount = $this->bankAccountFactory->create(
            $command->getName(),
            $command->getFirstName(),
            $command->getLastName(),
            $command->getUserId()
        );
        $this->bankAccountRepository->register($bankAccount);
    }
}