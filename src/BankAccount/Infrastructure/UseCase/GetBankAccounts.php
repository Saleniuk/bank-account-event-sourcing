<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator\BankAccountHydratorInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter\GetBankAccountsPresenterInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\BankAccountRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response\GetBankAccountsResponse;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase\GetBankAccountsInterface;

/**
 * Class GetBankAccounts
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase
 */
class GetBankAccounts implements GetBankAccountsInterface
{
    /** @var BankAccountRepositoryInterface */
    protected $bankAccountRepository;

    /** @var BankAccountHydratorInterface */
    protected $bankAccountHydrator;

    /**
     * GetBankAccounts constructor.
     * @param BankAccountRepositoryInterface $bankAccountRepository
     */
    public function __construct(
        BankAccountRepositoryInterface $bankAccountRepository,
        BankAccountHydratorInterface $bankAccountHydrator
    )
    {
        $this->bankAccountRepository = $bankAccountRepository;
        $this->bankAccountHydrator = $bankAccountHydrator;
    }

    /**
     * @param GetBankAccountsPresenterInterface $presenter
     * @return void
     */
    public function execute(GetBankAccountsPresenterInterface $presenter)
    {
        $response = new GetBankAccountsResponse();
        $bankAccounts = $this->bankAccountRepository->findByUserId(1);
        foreach ($bankAccounts as $bankAccount) {
            $response->addBankAccount(
                $this->bankAccountHydrator->extract($bankAccount)
            );
        }
        $presenter->present($response);
    }
}