<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\UnloadBankAccountCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\BankAccountRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase\UnloadBankAccountInterface;

/**
 * Class UnloadBankAccount
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase
 */
class UnloadBankAccount implements UnloadBankAccountInterface
{
    /** @var BankAccountRepositoryInterface */
    protected $bankAccountRepository;

    /**
     * UnloadBankAccount constructor.
     * @param BankAccountRepositoryInterface $bankAccountRepository
     */
    public function __construct(BankAccountRepositoryInterface $bankAccountRepository)
    {
        $this->bankAccountRepository = $bankAccountRepository;
    }

    /**
     * @param UnloadBankAccountCommand $command
     * @return void
     */
    public function execute(UnloadBankAccountCommand $command)
    {
        $this->bankAccountRepository->unload($command->getBankAccountId(), $command->getAmount());
    }
}