<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\CloseBankAccountCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\BankAccountRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase\CloseBankAccountInterface;

class CloseBankAccount implements CloseBankAccountInterface
{
    /** @var BankAccountRepositoryInterface */
    protected $bankAccountRepository;

    /**
     * CloseBankAccount constructor.
     * @param BankAccountRepositoryInterface $bankAccountRepository
     */
    public function __construct(BankAccountRepositoryInterface $bankAccountRepository)
    {
        $this->bankAccountRepository = $bankAccountRepository;
    }

    /**
     * @param CloseBankAccountCommand $command
     * @return void
     */
    public function execute(CloseBankAccountCommand $command)
    {
        $this->bankAccountRepository->close($command->getAggregateId());
    }
}