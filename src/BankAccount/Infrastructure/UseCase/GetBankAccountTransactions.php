<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\GetBankAccountTransactionsCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator\TransactionHydratorInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter\GetBankAccountsPresenterInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter\GetBankAccountTransactionsPresenterInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\BankAccountRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository\TransactionRepositoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response\GetBankAccountTransactionsResponse;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase\GetBankAccountTransactionsInterface;

class GetBankAccountTransactions implements GetBankAccountTransactionsInterface
{
    /** @var TransactionRepositoryInterface */
    protected $transactionRepository;

    /** @var TransactionHydratorInterface */
    protected $transactionHydrator;

    /**
     * GetBankAccountTransactions constructor.
     * @param TransactionRepositoryInterface $transactionRepository
     * @param TransactionHydratorInterface $transactionHydrator
     */
    public function __construct(
        TransactionRepositoryInterface $transactionRepository,
        TransactionHydratorInterface $transactionHydrator
    )
    {
        $this->transactionRepository = $transactionRepository;
        $this->transactionHydrator = $transactionHydrator;
    }

    /**
     * @param GetBankAccountTransactionsCommand $command
     * @param GetBankAccountTransactionsPresenterInterface $presenter
     * @return void
     */
    public function execute(
        GetBankAccountTransactionsCommand $command,
        GetBankAccountTransactionsPresenterInterface $presenter
    )
    {
        $response = new GetBankAccountTransactionsResponse();
        $transactions = $this->transactionRepository->findByBankAccountId($command->getBankAccountId());
        foreach ($transactions as $transaction) {
            $response->addTransaction(
                $this->transactionHydrator->extract($transaction)
            );
        }

        $presenter->present($response);
    }
}