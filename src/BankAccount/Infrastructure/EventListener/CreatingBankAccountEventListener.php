<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\EventListener;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasClosed;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasLoaded;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasRegistered;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasUnloaded;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\ReadModelFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\ReadModelRepositoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Service\CreateProjectionServiceInterface;

/**
 * Class CreatingBankAccountEventListener
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\EventListener
 */
class CreatingBankAccountEventListener
{
    /** @var ReadModelRepositoryInterface */
    protected $readModelRepository;

    /** @var ReadModelFactoryInterface */
    protected $readModelFactory;

    /** @var CreateProjectionServiceInterface */
    protected $createProjectionService;

    /**
     * CreatingBankAccountEventListener constructor.
     * @param ReadModelRepositoryInterface $readModelRepository
     * @param ReadModelFactoryInterface $readModelFactory
     * @param CreateProjectionServiceInterface $createProjectionService
     */
    public function __construct(ReadModelRepositoryInterface $readModelRepository, ReadModelFactoryInterface $readModelFactory, CreateProjectionServiceInterface $createProjectionService)
    {
        $this->readModelRepository = $readModelRepository;
        $this->readModelFactory = $readModelFactory;
        $this->createProjectionService = $createProjectionService;
    }

    /**
     * @param EventInterface $event
     */
    public function applyEvent(EventInterface $event)
    {
        $aggregate = $this->createProjectionService->execute($event);
        $readModel = $this->readModelFactory->create(
            'bank-account', ['id' => $event->getAggregateId(), 'user_id' => (string)$aggregate->getUserId()],
            $aggregate, $event
        );
        $this->readModelRepository->remove('bank-account', $event->getAggregateId(), $event->getId());
        $this->readModelRepository->save($readModel);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            BankAccountWasRegistered::class,
            CreatingBankAccountEventListener::class . '@applyEvent'
        );

        $events->listen(
            BankAccountWasLoaded::class,
            CreatingBankAccountEventListener::class . '@applyEvent'
        );

        $events->listen(
            BankAccountWasUnloaded::class,
            CreatingBankAccountEventListener::class . '@applyEvent'
        );

        $events->listen(
            BankAccountWasClosed::class,
            CreatingBankAccountEventListener::class . '@applyEvent'
        );
    }
}