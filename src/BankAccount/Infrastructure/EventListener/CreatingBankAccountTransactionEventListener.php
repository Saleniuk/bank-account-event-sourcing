<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\EventListener;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasLoaded;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event\BankAccountWasUnloaded;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory\ReadModelFactory;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\ReadModelRepositoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Service\CreateProjectionServiceInterface;

class CreatingBankAccountTransactionEventListener
{
    /** @var ReadModelRepositoryInterface */
    protected $readModelRepository;

    /** @var ReadModelFactory */
    protected $readModelFactory;

    /** @var CreateProjectionServiceInterface */
    protected $createProjectionService;

    /**
     * CreatingBankAccountTransactionEventListener constructor.
     * @param ReadModelRepositoryInterface $readModelRepository
     * @param ReadModelFactory $readModelFactory
     * @param CreateProjectionServiceInterface $createProjectionService
     */
    public function __construct(ReadModelRepositoryInterface $readModelRepository, ReadModelFactory $readModelFactory, CreateProjectionServiceInterface $createProjectionService)
    {
        $this->readModelRepository = $readModelRepository;
        $this->readModelFactory = $readModelFactory;
        $this->createProjectionService = $createProjectionService;
    }

    /**
     * @param EventInterface $event
     */
    public function applyEvent(EventInterface $event)
    {
        $aggregate = $this->createProjectionService->execute($event);
        $readModel = $this->readModelFactory->create('bank-account-transactions', ['id' => $event->getAggregateId()], $aggregate, $event);
        $this->readModelRepository->remove('bank-account-transactions', $event->getAggregateId(), $event->getId());
        $this->readModelRepository->save($readModel);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            BankAccountWasLoaded::class,
            CreatingBankAccountTransactionEventListener::class . '@applyEvent'
        );

        $events->listen(
            BankAccountWasUnloaded::class,
            CreatingBankAccountTransactionEventListener::class . '@applyEvent'
        );
    }
}