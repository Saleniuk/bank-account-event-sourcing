<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Hydrator;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator\BankAccountHydratorInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\BankAccountFields;

/**
 * Class BankAccountHydrator
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Hydrator
 */
class BankAccountHydrator implements BankAccountHydratorInterface
{
    /**
     * @param array $bankAccountData
     * @return BankAccount
     */
    public function hydrate(array $bankAccountData): BankAccount
    {
        return new BankAccount(
            $bankAccountData[BankAccountFields::ID],
            $bankAccountData[BankAccountFields::NAME],
            $bankAccountData[BankAccountFields::FIRST_NAME],
            $bankAccountData[BankAccountFields::LAST_NAME],
            $bankAccountData[BankAccountFields::USER_ID],
            $bankAccountData[BankAccountFields::BALANCE],
            $bankAccountData[BankAccountFields::STATUS]
        );
    }

    /**
     * @param BankAccount $bankAccount
     * @return array
     */
    public function extract(BankAccount $bankAccount): array
    {
        return [
            BankAccountFields::ID => $bankAccount->getId(),
            BankAccountFields::NAME => $bankAccount->getName(),
            BankAccountFields::FIRST_NAME => $bankAccount->getFirstName(),
            BankAccountFields::LAST_NAME => $bankAccount->getLastName(),
            BankAccountFields::USER_ID => $bankAccount->getUserId(),
            BankAccountFields::BALANCE => $bankAccount->getBalance(),
            BankAccountFields::STATUS => $bankAccount->getStatus()
        ];
    }
}