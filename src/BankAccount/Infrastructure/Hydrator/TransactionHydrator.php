<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Hydrator;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\Transaction;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator\TransactionHydratorInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\TransactionFields;

/**
 * Class TransactionHydrator
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Hydrator
 */
class TransactionHydrator implements TransactionHydratorInterface
{
    /**
     * @param array $transactionData
     * @return Transaction
     */
    public function hydrate(array $transactionData): Transaction
    {
        return new Transaction(
            $transactionData[TransactionFields::ID] ?? 0,
            $transactionData[TransactionFields::AMOUNT] ?? 0,
            $transactionData[TransactionFields::DESCRIPTION] ?? '',
            $transactionData[TransactionFields::TYPE] ?? ''
        );
    }

    /**
     * @param Transaction $transaction
     * @return array
     */
    public function extract(Transaction $transaction): array
    {
        return [
            TransactionFields::ID => $transaction->getId(),
            TransactionFields::AMOUNT => $transaction->getAmount(),
            TransactionFields::DESCRIPTION => $transaction->getDescription(),
            TransactionFields::TYPE => $transaction->getType()
        ];
    }
}