<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper;

class BankAccountWasRegisteredFields
{
    const AGGREGATE_ID = 'aggregate_id';
    const USER_ID = 'user_id';
    const NAME = 'name';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
}