<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper;

class BankAccountWasLoadedFields
{
    const AGGREGATE_ID = 'aggregate_id';
    const AMOUNT = 'amount';
}