<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper;

class BankAccountWasUnloadedFields
{
    const AGGREGATE_ID = 'aggregate_id';
    const AMOUNT = 'amount';
}