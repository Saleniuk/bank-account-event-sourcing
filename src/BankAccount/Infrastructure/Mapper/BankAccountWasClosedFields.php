<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Mapper;

class BankAccountWasClosedFields
{
    const AGGREGATE_ID = 'aggregate_id';
}