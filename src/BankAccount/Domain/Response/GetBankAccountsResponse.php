<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response;

/**
 * Class GetBankAccountsResponse
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response
 */
class GetBankAccountsResponse
{
    /** @var array */
    private $bankAccounts = [];

    /**
     * @param array $bankAccount
     */
    public function addBankAccount(array $bankAccount)
    {
        array_push($this->bankAccounts, $bankAccount);
    }

    /**
     * @return array
     */
    public function getBankAccounts(): array
    {
        return $this->bankAccounts;
    }
}