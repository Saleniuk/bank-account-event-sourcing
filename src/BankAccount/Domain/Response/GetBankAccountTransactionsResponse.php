<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response;

/**
 * Class GetBankAccountTransactionsResponse
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response
 */
class GetBankAccountTransactionsResponse
{
    /** @var array */
    private $transactions = [];

    /**
     * @param array $transaction
     */
    public function addTransaction(array $transaction)
    {
        array_push($this->transactions, $transaction);
    }

    /**
     * @return array
     */
    public function getTransactions(): array
    {
        return $this->transactions;
    }
}