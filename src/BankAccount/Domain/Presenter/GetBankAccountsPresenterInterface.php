<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response\GetBankAccountsResponse;

/**
 * Interface GetBankAccountsPresenterInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter
 */
interface GetBankAccountsPresenterInterface
{
    /**
     * @param GetBankAccountsResponse $response
     * @return mixed
     */
    public function present(GetBankAccountsResponse $response);

    /**
     * @return mixed
     */
    public function getView();
}