<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response\GetBankAccountTransactionsResponse;

/**
 * Interface GetBankAccountTransactionsPresenterInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter
 */
interface GetBankAccountTransactionsPresenterInterface
{
    /**
     * @param GetBankAccountTransactionsResponse $response
     * @return mixed
     */
    public function present(GetBankAccountTransactionsResponse $response);

    /**
     * @return mixed
     */
    public function getView();
}