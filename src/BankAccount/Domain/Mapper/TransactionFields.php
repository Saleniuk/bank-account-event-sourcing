<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper;

class TransactionFields
{
    const ID = 'id';
    const AMOUNT = 'amount';
    const DESCRIPTION = 'description';
    const TYPE = 'type';
    const TRANSACTIONS = 'transactions';
}