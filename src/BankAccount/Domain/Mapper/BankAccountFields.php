<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper;

class BankAccountFields
{
    const ID = 'id';
    const USER_ID = 'user_id';
    const NAME = 'name';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const BALANCE = 'balance';
    const STATUS = 'status';
    const BANK_ACCOUNTS = 'bank_accounts';
}