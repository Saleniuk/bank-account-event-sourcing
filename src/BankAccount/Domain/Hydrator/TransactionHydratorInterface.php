<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\Transaction;

/**
 * Interface TransactionHydratorInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator
 */
interface TransactionHydratorInterface
{
    /**
     * @param array $transactionData
     * @return Transaction
     */
    public function hydrate(array $transactionData): Transaction;

    /**
     * @param Transaction $transaction
     * @return array
     */
    public function extract(Transaction $transaction): array;
}