<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;

/**
 * Interface BankAccountHydratorInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Hydrator
 */
interface BankAccountHydratorInterface
{
    /**
     * @param array $bankAccountData
     * @return BankAccount
     */
    public function hydrate(array $bankAccountData): BankAccount;

    /**
     * @param BankAccount $bankAccount
     * @return array
     */
    public function extract(BankAccount $bankAccount): array;
}