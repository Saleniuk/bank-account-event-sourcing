<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Mapper\BankAccountFields;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;

/**
 * Class BankAccount
 */
class BankAccount implements AggregateInterface
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var int */
    private $userId;

    /** @var int */
    private $balance;

    /** @var int */
    private $status;

    /**
     * BankAccount constructor.
     * @param string $id
     * @param string $name
     * @param string $firstName
     * @param string $lastName
     * @param int $userId
     * @param int $balance
     * @param int $status
     */
    public function __construct($id, $name, $firstName, $lastName, $userId, $balance, $status)
    {
        $this->id = $id;
        $this->name = $name;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->userId = $userId;
        $this->balance = $balance;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        return $this->balance;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param int $amount
     */
    public function loadBankAccount(int $amount)
    {
        $this->balance += $amount;
    }

    /**
     * @param int $amount
     */
    public function unloadBankAccount(int $amount)
    {
        $this->balance -= $amount;
    }

    /**
     *
     */
    public function closeBankAccount()
    {
        $this->status = 2;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            BankAccountFields::ID => $this->getId(),
            BankAccountFields::USER_ID => $this->getUserId(),
            BankAccountFields::BALANCE => $this->getBalance(),
            BankAccountFields::STATUS => $this->getStatus(),
            BankAccountFields::NAME => $this->getName(),
            BankAccountFields::LAST_NAME => $this->getLastName(),
            BankAccountFields::FIRST_NAME => $this->getFirstName()
        ];
    }
}