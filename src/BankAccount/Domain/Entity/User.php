<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity;

/**
 * Class User
 */
class User
{
    /** @var int */
    private $id;

    /**
     * User constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}