<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity;

/**
 * Class Transaction
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity
 */
class Transaction
{
    const LOAD = 'load';
    const UNLOAD = 'unload';

    /** @var int */
    private $id;

    /** @var int */
    private $amount;

    /** @var string */
    private $description;

    /** @var string */
    private $type;

    /**
     * Transaction constructor.
     * @param int $id
     * @param int $amount
     * @param string $description
     * @param string $type
     */
    public function __construct($id, $amount, $description, $type)
    {
        $this->id = $id;
        $this->amount = $amount;
        $this->description = $description;
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}