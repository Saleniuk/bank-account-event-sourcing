<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command;

/**
 * Class CloseBankAccountCommand
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command
 */
class CloseBankAccountCommand
{
    /** @var string */
    private $aggregateId;

    /**
     * CloseBankAccountCommand constructor.
     * @param string $aggregateId
     */
    public function __construct($aggregateId)
    {
        $this->aggregateId = $aggregateId;
    }

    /**
     * @return string
     */
    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }
}