<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command;

/**
 * Class RegisterBankAccountCommand
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command
 */
class RegisterBankAccountCommand
{
    /** @var int */
    private $userId;

    /** @var string */
    private $name;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /**
     * RegisterBankAccountCommand constructor.
     * @param int $userId
     * @param string $name
     * @param string $firstName
     * @param string $lastName
     */
    public function __construct($userId, $name, $firstName, $lastName)
    {
        $this->userId = $userId;
        $this->name = $name;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }
}