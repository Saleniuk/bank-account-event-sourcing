<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command;

/**
 * Class GetBankAccountTransactionsCommand
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command
 */
class GetBankAccountTransactionsCommand
{
    /** @var string */
    private $bankAccountId;

    /**
     * GetBankAccountTransactionsCommand constructor.
     * @param string $bankAccountId
     */
    public function __construct($bankAccountId)
    {
        $this->bankAccountId = $bankAccountId;
    }

    /**
     * @return string
     */
    public function getBankAccountId(): string
    {
        return $this->bankAccountId;
    }
}