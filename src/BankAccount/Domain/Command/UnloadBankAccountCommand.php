<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command;

/**
 * Class UnloadBankAccountCommand
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command
 */
class UnloadBankAccountCommand
{
    /** @var string */
    private $bankAccountId;

    /** @var int */
    private $amount;

    /**
     * UnloadBankAccountCommand constructor.
     * @param string $bankAccountId
     * @param int $amount
     */
    public function __construct($bankAccountId, $amount)
    {
        $this->bankAccountId = $bankAccountId;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getBankAccountId(): string
    {
        return $this->bankAccountId;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }
}