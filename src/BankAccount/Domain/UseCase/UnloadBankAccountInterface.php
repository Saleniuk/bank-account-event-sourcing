<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\UnloadBankAccountCommand;

/**
 * Interface UnloadBankAccountInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase
 */
interface UnloadBankAccountInterface
{
    /**
     * @param UnloadBankAccountCommand $command
     * @return mixed
     */
    public function execute(UnloadBankAccountCommand $command);
}