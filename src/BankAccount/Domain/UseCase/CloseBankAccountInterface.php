<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\CloseBankAccountCommand;

/**
 * Interface CloseBankAccountInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase
 */
interface CloseBankAccountInterface
{
    /**
     * @param CloseBankAccountCommand $command
     * @return mixed
     */
    public function execute(CloseBankAccountCommand $command);
}