<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\GetBankAccountTransactionsCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter\GetBankAccountTransactionsPresenterInterface;

/**
 * Interface GetBankAccountTransactionsInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase
 */
interface GetBankAccountTransactionsInterface
{
    /**
     * @param GetBankAccountTransactionsCommand $command
     * @param GetBankAccountTransactionsPresenterInterface $presenter
     * @return void
     */
    public function execute(GetBankAccountTransactionsCommand $command, GetBankAccountTransactionsPresenterInterface $presenter);
}