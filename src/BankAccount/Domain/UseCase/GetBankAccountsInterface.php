<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter\GetBankAccountsPresenterInterface;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Response\GetBankAccountsResponse;

/**
 * Interface GetBankAccountsInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase
 */
interface GetBankAccountsInterface
{
    /**
     * @param GetBankAccountsPresenterInterface $presenter
     * @return void
     */
    public function execute(GetBankAccountsPresenterInterface $presenter);
}