<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\RegisterBankAccountCommand;
use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Presenter\RegisterBankAccountPresenter;

/**
 * Interface RegisterBankAccountInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase
 */
interface RegisterBankAccountInterface
{
    /**
     * @param RegisterBankAccountCommand $command
     * @return void
     */
    public function execute(RegisterBankAccountCommand $command);
}