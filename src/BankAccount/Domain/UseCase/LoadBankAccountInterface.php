<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Command\LoadBankAccountCommand;

/**
 * Interface LoadBankAccountInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\UseCase
 */
interface LoadBankAccountInterface
{
    /**
     * @param LoadBankAccountCommand $command
     * @return void
     */
    public function execute(LoadBankAccountCommand $command);
}