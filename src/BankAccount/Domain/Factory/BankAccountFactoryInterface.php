<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;

/**
 * Interface BankAccountFactoryInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory
 */
interface BankAccountFactoryInterface
{
    /**
     * @param string $name
     * @param string $firstName
     * @param string $lastName
     * @param int $userId
     * @return BankAccount
     */
    public function create(string $name, string $firstName, string $lastName, int $userId): BankAccount;
}