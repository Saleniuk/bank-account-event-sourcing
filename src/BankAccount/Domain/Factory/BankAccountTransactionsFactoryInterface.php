<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory;

use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;

/**
 * Interface BankAccountTransactionsFactoryInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory
 */
interface BankAccountTransactionsFactoryInterface
{
    /**
     * @return AggregateInterface
     */
    public function create(): AggregateInterface;
}