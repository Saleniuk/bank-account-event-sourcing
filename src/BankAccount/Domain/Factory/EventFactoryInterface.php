<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory;

use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Interface EventFactoryInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory
 */
interface EventFactoryInterface
{
    /**
     * @param string $name
     * @param array $eventData
     * @return EventInterface
     */
    public function create(string $name, array $eventData): EventInterface;
}