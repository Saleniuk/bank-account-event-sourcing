<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository;

/**
 * Interface TransactionRepositoryInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository
 */
interface TransactionRepositoryInterface
{
    /**
     * @param string $bankAccountId
     * @return array
     */
    public function findByBankAccountId(string $bankAccountId): array;
}