<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Entity\BankAccount;

/**
 * Interface BankAccountRepositoryInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Repository
 */
interface BankAccountRepositoryInterface
{
    /**
     * @param int $id
     * @return BankAccount
     */
    public function findById(int $id): BankAccount;

    /**
     * @param int $userId
     * @return array
     */
    public function findByUserId(int $userId): array;

    /**
     * @param BankAccount $bankAccount
     * @return int
     */
    public function register(BankAccount $bankAccount);

    /**
     * @param string $bankAccountId
     * @param int $amount
     * @return void
     */
    public function load(string $bankAccountId, int $amount);

    /**
     * @param string $bankAccountId
     * @param int $amount
     * @return void
     */
    public function unload(string $bankAccountId, int $amount);

    /**
     * @param string $bankAccountId
     * @return void
     */
    public function close(string $bankAccountId);
}