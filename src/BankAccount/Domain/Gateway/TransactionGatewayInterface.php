<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Gateway;

/**
 * Interface TransactionGatewayInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Gateway
 */
interface TransactionGatewayInterface
{
    /**
     * @param string $bankAccountId
     * @return array
     */
    public function findByBankAccountId(string $bankAccountId): array;
}