<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Gateway;

/**
 * Interface BankAccountGatewayInterface
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Gateway
 */
interface BankAccountGatewayInterface
{
    /**
     * @param int $id
     * @return array
     */
    public function findById(int $id): array;

    /**
     * @param array $bankAccountData
     * @return mixed
     */
    public function register(array $bankAccountData);

    /**
     * @param int $bankAccountId
     * @param int $amount
     * @return mixed
     */
    public function load(int $bankAccountId, int $amount);

    /**
     * @param array $bankAccountData
     * @param int $amount
     * @return mixed
     */
    public function unload(array $bankAccountData, int $amount);
}