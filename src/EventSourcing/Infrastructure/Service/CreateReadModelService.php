<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Service;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory\EventFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\ProjectorInterface;
use JakubSaleniuk\EventSourcing\Domain\Entity\EventStorage;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\AggregateFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\EventAggregateFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\ProjectorFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\ReadModelFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\EventStorageRepositoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\ReadModelRepositoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Service\CreateReadModelServiceInterface;

/**
 * Class CreateReadModelService
 * @package JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Service
 */
class CreateReadModelService implements CreateReadModelServiceInterface
{
    /** @var EventAggregateFactoryInterface */
    protected $eventAggregateFactory;

    /** @var EventStorageRepositoryInterface */
    protected $eventStorageRepository;

    /** @var EventFactoryInterface */
    protected $eventFactory;

    /** @var ProjectorFactoryInterface */
    protected $projectorFactory;

    /** @var ReadModelRepositoryInterface */
    protected $readModelRepository;

    /** @var ReadModelFactoryInterface */
    protected $readModelFactory;

    /**
     * CreateReadModelService constructor.
     * @param EventAggregateFactoryInterface $eventAggregateFactory
     * @param EventStorageRepositoryInterface $eventStorageRepository
     * @param EventFactoryInterface $eventFactory
     * @param ProjectorFactoryInterface $projectorFactory
     * @param ReadModelRepositoryInterface $readModelRepository
     * @param ReadModelFactoryInterface $readModelFactory
     */
    public function __construct(
        EventAggregateFactoryInterface $eventAggregateFactory,
        EventStorageRepositoryInterface $eventStorageRepository,
        EventFactoryInterface $eventFactory,
        ProjectorFactoryInterface $projectorFactory,
        ReadModelRepositoryInterface $readModelRepository,
        ReadModelFactoryInterface $readModelFactory
    ) {
        $this->eventAggregateFactory = $eventAggregateFactory;
        $this->eventStorageRepository = $eventStorageRepository;
        $this->eventFactory = $eventFactory;
        $this->projectorFactory = $projectorFactory;
        $this->readModelRepository = $readModelRepository;
        $this->readModelFactory = $readModelFactory;
    }

    /**
     * @param EventInterface $event
     * @param AggregateInterface $aggregate
     * @return void
     */
    public function createReadModel(EventInterface $event, AggregateInterface $aggregate)
    {
        $eventStorages = $this->eventStorageRepository->getByAggregateId($event->getAggregateId());
        /** @var ProjectorInterface $projector */
        $projector = $this->projectorFactory->create($aggregate);
        $eventAggregate = $this->eventAggregateFactory->create();
        /** @var EventStorage $eventStorage */
        foreach ($eventStorages as $eventStorage) {
            $eventAggregate->addEvent(
                $this->eventFactory->create($eventStorage->getName(), $eventStorage->getPayload())
            );
        }

        $aggregate = $projector->applyEvents($eventAggregate);
        $readModel = $this->readModelFactory->create($projector->getProjectionName(), $metadata, $aggregate, $event);
        $this->readModelRepository->remove('bank-account', $event->getAggregateId(), $event->getId());
        $this->readModelRepository->save($readModel);
    }
}