<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Service;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Domain\Factory\EventFactoryInterface;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory\EventAggregateFactory;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\AggregateFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\ProjectorFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\EventStorageRepositoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Service\CreateProjectionServiceInterface;

/**
 * Class CreateProjectionService
 * @package JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Service
 */
class CreateProjectionService implements CreateProjectionServiceInterface
{
    /** @var EventStorageRepositoryInterface */
    protected $eventStorageRepository;

    /** @var ProjectorFactoryInterface */
    protected $projectorFactory;

    /** @var EventFactoryInterface */
    protected $eventFactory;

    /** @var AggregateFactoryInterface */
    protected $aggregateFactory;

    /** @var EventAggregateFactory */
    protected $eventAggregateFactory;

    /**
     * CreateProjectionService constructor.
     * @param EventStorageRepositoryInterface $eventStorageRepository
     * @param ProjectorFactoryInterface $projectorFactory
     * @param EventFactoryInterface $eventFactory
     * @param AggregateFactoryInterface $aggregateFactory
     * @param EventAggregateFactory $eventAggregateFactory
     */
    public function __construct(EventStorageRepositoryInterface $eventStorageRepository, ProjectorFactoryInterface $projectorFactory, EventFactoryInterface $eventFactory, AggregateFactoryInterface $aggregateFactory, EventAggregateFactory $eventAggregateFactory)
    {
        $this->eventStorageRepository = $eventStorageRepository;
        $this->projectorFactory = $projectorFactory;
        $this->eventFactory = $eventFactory;
        $this->aggregateFactory = $aggregateFactory;
        $this->eventAggregateFactory = $eventAggregateFactory;
    }

    /**
     * @param EventInterface $event
     * @return AggregateInterface
     */
    public function execute(EventInterface $event): AggregateInterface
    {
        $aggregate = $this->aggregateFactory->createAggregate();
        $eventStorages = $this->eventStorageRepository->getByAggregateId($event->getAggregateId());
        $projector = $this->projectorFactory->create($aggregate);
        $eventAggregate = $this->eventAggregateFactory->create();
        /** @var EventStorage $eventStorage */
        foreach ($eventStorages as $eventStorage) {
            $eventAggregate->addEvent(
                $this->eventFactory->create($eventStorage->getName(), $eventStorage->getPayload())
            );
        }
        /** @var TransactionAggregate $transactions */
        $aggregate = $projector->applyEvents($eventAggregate);

        return $aggregate;
    }
}