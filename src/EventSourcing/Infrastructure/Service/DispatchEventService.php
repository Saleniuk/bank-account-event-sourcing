<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Service;

use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;
use JakubSaleniuk\EventSourcing\Domain\Event\EventBusInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\EventStorageFactoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\EventStorageRepositoryInterface;
use JakubSaleniuk\EventSourcing\Domain\Service\DispatchEventServiceInterface;

/**
 * Class DispatchEventService
 * @package JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Service
 */
class DispatchEventService implements DispatchEventServiceInterface
{
    /** @var EventStorageFactoryInterface */
    protected $eventStorageFactory;

    /** @var EventStorageRepositoryInterface */
    protected $eventStorageRepository;

    /** @var EventBusInterface */
    protected $eventBus;

    /**
     * DispatchEventService constructor.
     * @param EventStorageFactoryInterface $eventStorageFactory
     * @param EventStorageRepositoryInterface $eventStorageRepository
     * @param EventBusInterface $eventBus
     */
    public function __construct(
        EventStorageFactoryInterface $eventStorageFactory,
        EventStorageRepositoryInterface $eventStorageRepository,
        EventBusInterface $eventBus
    ) {
        $this->eventStorageFactory = $eventStorageFactory;
        $this->eventStorageRepository = $eventStorageRepository;
        $this->eventBus = $eventBus;
    }

    /**
     * @param EventInterface $event
     * @return void
     */
    public function dispatch(EventInterface $event)
    {
        $eventStorage = $this->eventStorageFactory->create($event);
        $id = $this->eventStorageRepository->save($eventStorage);
        $event->setId($id);
        $this->eventBus->dispatch($event);
    }
}