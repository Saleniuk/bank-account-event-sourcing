<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory;

use JakubSaleniuk\EventSourcing\Domain\Entity\EventStorage;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\EventStorageFactoryInterface;

/**
 * Class EventStorageFactory
 * @package JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory
 */
class EventStorageFactory implements EventStorageFactoryInterface
{
    /**
     * @param EventInterface $event
     * @return EventStorage
     */
    public function create(EventInterface $event): EventStorage
    {
        return new EventStorage(
            0,
            uniqid('event_'),
            $event->getEventName(),
            $event->getAggregateId(),
            1, // toDo: counter
            $event->toArray(),
            []
        );
    }
}