<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory;

use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Entity\ReadModel;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\ReadModelFactoryInterface;

/**
 * Class ReadModelFactory
 * @package JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory
 */
class ReadModelFactory implements ReadModelFactoryInterface
{
    /**
     * @param string $name
     * @param array $metadata
     * @param AggregateInterface $aggregate
     * @param EventInterface $event
     * @return ReadModel
     */
    public function create(
        string $name,
        array $metadata,
        AggregateInterface $aggregate,
        EventInterface $event
    ): ReadModel
    {
        return new ReadModel(
            0,
            $metadata,
            $name,
            $aggregate->toArray(),
            $event->getId()
        );
    }
}