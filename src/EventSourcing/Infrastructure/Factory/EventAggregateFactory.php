<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory;

use JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Aggregate\EventAggregate;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\EventAggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Factory\EventAggregateFactoryInterface;

/**
 * Class EventAggregateFactory
 * @package JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Factory
 */
class EventAggregateFactory implements EventAggregateFactoryInterface
{
    /**
     * @return EventAggregateInterface
     */
    public function create(): EventAggregateInterface
    {
        return new EventAggregate();
    }
}