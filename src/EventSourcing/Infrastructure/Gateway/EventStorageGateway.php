<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Gateway;

use App\Model\EventStorage;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Mapper\EventStorageFields;
use JakubSaleniuk\EventSourcing\Domain\Gateway\EventStorageGatewayInterface;

/**
 * Class EventStorageGateway
 * @package JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Gateway
 */
class EventStorageGateway implements EventStorageGatewayInterface
{
    /**
     * @param string $aggregateId
     * @return array
     */
    public function getByAggregateId(string $aggregateId): array
    {
        $eventStorages = EventStorage::where('aggregate_id', $aggregateId)->get();

        return $eventStorages->toArray();
    }

    /**
     * @param array $eventStorageData
     * @return int
     */
    public function save(array $eventStorageData): int
    {
        $eventStorage = new EventStorage();
        $eventStorage->uuid = $eventStorageData[EventStorageFields::UUID];
        $eventStorage->aggregate_id = $eventStorageData[EventStorageFields::AGGREGATE_ID];
        $eventStorage->name = $eventStorageData[EventStorageFields::NAME];
        $eventStorage->metadata = $eventStorageData[EventStorageFields::METADATA];
        $eventStorage->counter = $eventStorageData[EventStorageFields::COUNTER];
        $eventStorage->payload = $eventStorageData[EventStorageFields::PAYLOAD];
        $eventStorage->save();

        return $eventStorage->id;
    }
}