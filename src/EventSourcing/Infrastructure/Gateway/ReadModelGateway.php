<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Gateway;

use App\Model\ReadModel;
use Illuminate\Support\Facades\DB;
use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Mapper\ReadModelFields;
use JakubSaleniuk\EventSourcing\Domain\Gateway\ReadModelGatewayInterface;

/**
 * Class ReadModelGateway
 * @package JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Gateway
 */
class ReadModelGateway implements ReadModelGatewayInterface
{
    /**
     * @param string $name
     * @param array $condition
     * @return array
     */
    public function find(string $name, array $condition): array
    {
        $readModel = DB::table('read_model');

        $readModel->where('name', $name);
        foreach ($condition['metadata'] as $key => $value) {
            $readModel->where('metadata->' . $key, (string)$value);
        }
        $results = $readModel->get()->all();

        $readModels = [];
        foreach ($results as $readModel) {
            array_push($readModels, [
                ReadModelFields::ID => (int)$readModel->id,
                ReadModelFields::NAME => $readModel->name,
                ReadModelFields::METADATA => $readModel->metadata,
                ReadModelFields::PROJECTION => $readModel->projection,
                ReadModelFields::EVENT_ID => $readModel->event_id
            ]);
        }

        return $readModels;
    }

    /**
     * @param array $readModelData
     * @return int
     */
    public function save(array $readModelData): int
    {
        $readModel = new ReadModel();
        $readModel->name = $readModelData[ReadModelFields::NAME];
        $readModel->metadata = $readModelData[ReadModelFields::METADATA];
        $readModel->projection = $readModelData[ReadModelFields::PROJECTION];
        $readModel->event_id = $readModelData[ReadModelFields::EVENT_ID];
        $readModel->save();

        return $readModel->id;
    }

    /**
     * @param string $name
     * @param string $aggregateId
     * @param int $eventId
     */
    public function remove(string $name, string $aggregateId, int $eventId)
    {
        $readModel = DB::table('read_model');

        $readModel->where('name', $name);
        $readModel->where('metadata->id', (string)$aggregateId);
        $readModel->where('event_id', '<', $eventId);

        $readModel->delete();
    }
}