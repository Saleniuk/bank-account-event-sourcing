<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Event;

use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;
use JakubSaleniuk\EventSourcing\Domain\Event\EventBusInterface;

/**
 * Class EventBus
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Event
 */
class EventBus implements EventBusInterface
{
    /**
     * @param EventInterface $event
     */
    public function dispatch(EventInterface $event)
    {
        event($event);
    }
}