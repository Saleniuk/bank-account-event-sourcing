<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Hydrator;

use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Mapper\ReadModelFields;
use JakubSaleniuk\EventSourcing\Domain\Entity\ReadModel;
use JakubSaleniuk\EventSourcing\Domain\Hydrator\ReadModelHydratorInterface;

/**
 * Class ReadModelHydrator
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Hydrator
 */
class ReadModelHydrator implements ReadModelHydratorInterface
{
    /**
     * @param array $readModelData
     * @return ReadModel
     */
    public function hydrate(array $readModelData): ReadModel
    {
        return new ReadModel(
            $readModelData[ReadModelFields::ID] ?? 0,
            isset($readModelData[ReadModelFields::METADATA]) ? json_decode($readModelData[ReadModelFields::METADATA], true) : [],
            $readModelData[ReadModelFields::NAME] ?? '',
            isset($readModelData[ReadModelFields::PROJECTION]) ? json_decode($readModelData[ReadModelFields::PROJECTION], true) : [],
            $readModelData[ReadModelFields::EVENT_ID] ?? 0
        );
    }

    /**
     * @param ReadModel $readModel
     * @return array
     */
    public function extract(ReadModel $readModel): array
    {
        return [
            ReadModelFields::ID => $readModel->getId(),
            ReadModelFields::METADATA => json_encode($readModel->getMetadata()),
            ReadModelFields::NAME => $readModel->getName(),
            ReadModelFields::PROJECTION => json_encode($readModel->getProjection()),
            ReadModelFields::EVENT_ID => $readModel->getEventId()
        ];
    }
}