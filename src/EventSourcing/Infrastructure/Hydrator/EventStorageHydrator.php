<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Hydrator;

use JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Mapper\EventStorageFields;
use JakubSaleniuk\EventSourcing\Domain\Entity\EventStorage;
use JakubSaleniuk\EventSourcing\Domain\Hydrator\EventStorageHydratorInterface;

/**
 * Class EventStorageHydrator
 * @package JakubSaleniuk\BankAccountEventSourcing\BankAccount\Infrastructure\Hydrator
 */
class EventStorageHydrator implements EventStorageHydratorInterface
{
    /**
     * @param array $eventStorageData
     * @return EventStorage
     */
    public function hydrate(array $eventStorageData): EventStorage
    {
        return new EventStorage(
            $eventStorageData[EventStorageFields::ID] ?? 0,
            $eventStorageData[EventStorageFields::UUID] ?? '',
            $eventStorageData[EventStorageFields::NAME] ?? '',
            $eventStorageData[EventStorageFields::AGGREGATE_ID] ?? '',
            $eventStorageData[EventStorageFields::COUNTER] ?? 0,
            isset($eventStorageData[EventStorageFields::PAYLOAD]) ? json_decode($eventStorageData[EventStorageFields::PAYLOAD], true) : [],
            isset($eventStorageData[EventStorageFields::METADATA]) ? json_decode($eventStorageData[EventStorageFields::METADATA], true) : []
        );
    }

    /**
     * @param EventStorage $eventStorage
     * @return array
     */
    public function extract(EventStorage $eventStorage): array
    {
        return [
            EventStorageFields::ID => $eventStorage->getId(),
            EventStorageFields::METADATA => json_encode($eventStorage->getMetadata()),
            EventStorageFields::NAME => $eventStorage->getName(),
            EventStorageFields::AGGREGATE_ID => $eventStorage->getAggregateId(),
            EventStorageFields::PAYLOAD => json_encode($eventStorage->getPayload()),
            EventStorageFields::COUNTER => $eventStorage->getCounter(),
            EventStorageFields::UUID => $eventStorage->getUuid()
        ];
    }
}