<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Repository;

use JakubSaleniuk\EventSourcing\Domain\Entity\ReadModel;
use JakubSaleniuk\EventSourcing\Domain\Gateway\ReadModelGatewayInterface;
use JakubSaleniuk\EventSourcing\Domain\Hydrator\ReadModelHydratorInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\ReadModelRepositoryInterface;

/**
 * Class ReadModelRepository
 * @package JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Repository
 */
class ReadModelRepository implements ReadModelRepositoryInterface
{
    /** @var ReadModelGatewayInterface */
    protected $readModelGateway;

    /** @var ReadModelHydratorInterface */
    protected $readModelHydrator;

    /**
     * ReadModelRepository constructor.
     * @param ReadModelGatewayInterface $readModelGateway
     * @param ReadModelHydratorInterface $readModelHydrator
     */
    public function __construct(ReadModelGatewayInterface $readModelGateway, ReadModelHydratorInterface $readModelHydrator)
    {
        $this->readModelGateway = $readModelGateway;
        $this->readModelHydrator = $readModelHydrator;
    }

    /**
     * @param string $name
     * @param array $conditions
     * @return array
     */
    public function find(string $name, array $conditions): array
    {
        $readModels = [];
        $readModelsData = $this->readModelGateway->find($name, $conditions);

        foreach ($readModelsData as $readModel) {
            array_push($readModels, $this->readModelHydrator->hydrate($readModel));
        }

        return $readModels;
    }

    /**
     * @param ReadModel $readModel
     * @return mixed
     */
    public function save(ReadModel $readModel): int
    {
        return $this->readModelGateway->save(
            $this->readModelHydrator->extract($readModel)
        );
    }

    /**
     * @param string $name
     * @param string $aggregateId
     * @param int $eventId
     */
    public function remove(string $name, string $aggregateId, int $eventId)
    {
        $this->readModelGateway->remove($name, $aggregateId, $eventId);
    }
}