<?php

declare(strict_types = 1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Repository;

use JakubSaleniuk\EventSourcing\Domain\Entity\EventStorage;
use JakubSaleniuk\EventSourcing\Domain\Gateway\EventStorageGatewayInterface;
use JakubSaleniuk\EventSourcing\Domain\Hydrator\EventStorageHydratorInterface;
use JakubSaleniuk\EventSourcing\Domain\Repository\EventStorageRepositoryInterface;

/**
 * Class EventStorageRepository
 * @package JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Repository
 */
class EventStorageRepository implements EventStorageRepositoryInterface
{
    /** @var EventStorageGatewayInterface */
    protected $eventStorageGateway;

    /** @var EventStorageHydratorInterface */
    protected $eventStorageHydrator;

    /**
     * EventStorageRepository constructor.
     * @param EventStorageGatewayInterface $eventStorageGateway
     * @param EventStorageHydratorInterface $eventStorageHydrator
     */
    public function __construct(EventStorageGatewayInterface $eventStorageGateway, EventStorageHydratorInterface $eventStorageHydrator)
    {
        $this->eventStorageGateway = $eventStorageGateway;
        $this->eventStorageHydrator = $eventStorageHydrator;
    }

    /**
     * @param string $aggregateId
     * @return array
     */
    public function getByAggregateId(string $aggregateId): array
    {
        $eventStorages = [];
        $eventStoragesData = $this->eventStorageGateway->getByAggregateId($aggregateId);
        foreach($eventStoragesData as $eventStorage) {
            array_push($eventStorages, $this->eventStorageHydrator->hydrate($eventStorage));
        }

        return $eventStorages;
    }

    /**
     * @param EventStorage $eventStorage
     * @return int
     */
    public function save(EventStorage $eventStorage): int
    {
        return $this->eventStorageGateway->save(
            $this->eventStorageHydrator->extract($eventStorage)
        );
    }
}