<?php

declare(strict_types=1);

namespace JakubSaleniuk\BankAccountEventSourcing\EventSourcing\Infrastructure\Mapper;

class EventStorageFields
{
    const ID = 'id';
    const UUID = 'uuid';
    const NAME = 'name';
    const AGGREGATE_ID = 'aggregate_id';
    const COUNTER = 'counter';
    const PAYLOAD = 'payload';
    const METADATA = 'metadata';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}